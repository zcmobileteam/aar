# ZainSDK


####Payment SDK implementation for ZainCash

ZainPayment supports 3 locales: LOCALE_EN, LOCALE_AR, LOCALE_KU. Those constants can be used to 
pass the desired locale for the library screens to appear with. 
This field is optional(for Kotlin users) with value LOCALE_EN



Usage from kotlin:
 
```
 ZainPayment(
            this,
            publicKey = "MIIBITANBgkqhkiG9w0BAQEFAAOCAQ4AMIIBCQKCAQBGvNkLnetAtR+QSttxIkQ9mH7pbbjl2Uq" +
                    "Ru5UDO9kuEiYh4b70JxPN8v1exkuW/FLmxKjdRVq7gNWstumIGm1W8cf4RtFj88pvZUaVg6NZ21iLAI" +
                    "Htnhb2D/4eBOI8HXdhdZ+bEd+BJbu1rlqm0Rs11jzYukR35/n44me3fbP9DH3JmSM8s0F8RmlIY0Vq" +
                    "DnSOCOazNupVtJQFWeDIyfcV/coW+RRrFq5KNwnHPxdl5o3PR3OZgV27H/eBuKxIEGvjBUYchSjAAd" +
                    "JYAnfISvcdhuLeYocZGi5WHEswrQBoUG8GflcdMJTvtTL5PtJG2WdcurIQA6iD2fSdBgQpARJFAgMBA" +
                    "AE=",
            merchantCode = "test",
            isTesting = BuildConfig.DEBUG
        ).start(
            orderId = "1",
            amount = 200.0,
            onSuccess = {
                Toast.makeText(this, it.toString(), Toast.LENGTH_LONG).show()
            },
            onError = {
                Toast.makeText(this, it, Toast.LENGTH_LONG).show()
            },
            onCancel = {
                Toast.makeText(this, "Canceled", Toast.LENGTH_LONG).show()
            }, locale = ZainPayment.LOCALE_AR
        )
        
```
 
 Usage from Java:
 
```
 new ZainPayment(this,
                "MIIBITANBgkqhkiG9w0BAQEFAAOCAQ4AMIIBCQKCAQBGvNkLnetAtR+QSttxIkQ9mH7pbbjl2Uq" +
                        "Ru5UDO9kuEiYh4b70JxPN8v1exkuW/FLmxKjdRVq7gNWstumIGm1W8cf4RtFj88pvZUaVg6NZ21iLAI" +
                        "Htnhb2D/4eBOI8HXdhdZ+bEd+BJbu1rlqm0Rs11jzYukR35/n44me3fbP9DH3JmSM8s0F8RmlIY0Vq" +
                        "DnSOCOazNupVtJQFWeDIyfcV/coW+RRrFq5KNwnHPxdl5o3PR3OZgV27H/eBuKxIEGvjBUYchSjAAd" +
                        "JYAnfISvcdhuLeYocZGi5WHEswrQBoUG8GflcdMJTvtTL5PtJG2WdcurIQA6iD2fSdBgQpARJFAgMBA" +
                        "AE=",
                "test",
                BuildConfig.DEBUG)
                .start(
                        "1", 200d,
                        new Function1<PaymentTransaction, Void>() {
                            @Override
                            public Void invoke(PaymentTransaction paymentTransaction) {
                                Toast.makeText(SplashJavaActivity.this, paymentTransaction.toString(), Toast.LENGTH_LONG).show();
                                return null;
                            }
                        },
                        new Function1<String, Void>() {
                            @Override
                            public Void invoke(String error) {
                                Toast.makeText(SplashJavaActivity.this, error, Toast.LENGTH_LONG).show();
                                return null;
                            }
                        },
                        new Function0<Void>() {
                            @Override
                            public Void invoke() {
                                Toast.makeText(SplashJavaActivity.this, "Canceled", Toast.LENGTH_LONG).show();
                                return null;
                            }
                        }, ZainPayment.LOCALE_EN
                );
```
 
 Setting up the library is causing some issues. As a (ugly) work around please add the following 
 dependencies in your app.gradle file:
 
```
 implementation "androidx.constraintlayout:constraintlayout:2.0.0-beta2"
    implementation "com.google.android.material:material:1.1.0-alpha07"

    //JOSE encryption library
    implementation group: 'com.nimbusds', name: 'nimbus-jose-jwt', version: '5.1'

    // Koin for Kotlin
    implementation "org.koin:koin-core:$koin_version"
    // Koin for Android
    // Koin for Android
    implementation "org.koin:koin-android:$koin_version"
    // Koin AndroidX Scope features
    implementation "org.koin:koin-androidx-scope:2.0.1"
    // Koin AndroidX ViewModel features
    implementation "org.koin:koin-androidx-viewmodel:2.0.1"

    //networking
    implementation 'com.squareup.retrofit2:retrofit:2.6.0'
    implementation("com.squareup.moshi:moshi-kotlin:1.8.0")
    kapt("com.squareup.moshi:moshi-kotlin-codegen:1.8.0")
    implementation "com.squareup.moshi:moshi-adapters:1.8.0"
    implementation "com.squareup.retrofit2:converter-moshi:2.5.0"
    implementation("com.squareup.okhttp3:okhttp:4.0.0")

    implementation "org.jetbrains.kotlinx:kotlinx-coroutines-android:1.2.1"
    implementation 'org.jetbrains.kotlinx:kotlinx-coroutines-core:1.2.1'
    implementation "androidx.lifecycle:lifecycle-viewmodel-ktx:2.2.0-alpha02"

    implementation(name: 'payment-debug', ext: 'aar') {
        transitive = true
    }
```

Donwload aar file over here:

https://bitbucket.org/zcmobileteam/aar/downloads/payment-debug.aar

Demo

![App Gif demo](/screenshots/zaincash_payment_demo.gif)